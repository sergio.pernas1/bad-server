# Bad server



## Instrucciones

En un sistema "limpio" clone este repositorio y ejecute el script de despliegue.

```
$ sudo su -

# git clone https://gitlab.com/sergio.pernas1/bad-server.git

# cd bad-server

# bash deploy.sh
```

Cuando el proceso de despliegue termine el sistema se reiniciará, revise los logs de systemd y apache para resolver los problemas.



## Entrega

Cada fichero corregido se debe presentar por la plataforma 'pastebin' y debe incluir la linea o lineas erróneas comentadas junto con la corrección, por ejemplo:

```
...
#PrFile /path/to/pids/apache.pd
PidFile /path/to/pids/apache.pd
...
```

También deberá adjuntar un fichero con las tareas hechas a través de terminal, por ejemplo cambio de permisos, creación de directorios, estado de salida de systemd, etc. Este fichero debe mostrar que tareas se hicieron en linea de comandos, no incluir los comando 'vim apache2.conf' y del estilo.
